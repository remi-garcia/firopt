//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#pragma once

#include "ScaLP/Solver.h"
#include "adder_depth.h"
#include "cheby.h"
#include "compute_successor_set.h"
#include "types.h"
#include <limits>
#include <list>
#include <map>
#include <ostream>
#include <set>
#include <vector>

#ifndef FIROPT_LPCOEFFBOUND_H
#define FIROPT_LPCOEFFBOUND_H

class LPCoeffBound {
public:
  LPCoeffBound(list<string> solverWishList, int threads, int timeout,
               int noOfCoefficients, IRtype ir_type, int wordlength,
               vector<vector<double>> omega, int no_bands,
               vector<pair<double, double>> A_bands, bool useFixedGain);

  list<string> solverWishList;
  int threads;
  int timeout;

  int noOfCoefficients;
  IRtype ir_type;
  int wordlength;

  vector<pair<double, double>> A_bands;
  vector<vector<double>> omega;

  /*
   * This is the main function of this class;
   * it iteratively solves instances of LP problems
   * in order to determine the bounds on the _h_m, m=0...M-1
   *
   * In case of success, it returns 1 and the bounds are contained
   * in the public field h_bounds.
   * Otherwise, it returns 0.
   */
  int compute_bounds();

  /*
   * If compute_bounds() returns 1, contains the pairs of lower and upper
   * bounds on the impulse response _h_m.
   * Otherwise, contains intervals [+/- Inf].
   */
  vector<pair<int_t, int_t>> h_bounds;

private:
  bool useFixedGain;
  int _no_bands;
  vector<ScaLP::Variable> _h_m;

  list<ScaLP::Constraint *> _terms_C1;

  /* Container for the basis functions */
  double_fun_vec_t _basis;

  vector<function<double(double, double)>> _bounds;

  void _create_basis();
  void _create_IR_bounds();
  void _create_constraint_C1();

  ScaLP::Variable gain;
};

#endif // FIROPT_LPCOEFFBOUND_H
