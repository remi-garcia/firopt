//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "FIROpt.h"
#include "ScaLP/SolverDynamic.h"
#include "fundamental.h"
#include <cmath>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>
#include <iomanip>
#include <iostream>
#include <limits>
#include <sstream>

using std::list;
using std::string;

FIROpt::FIROpt(list<string> solverWishList, int threads, int timeout,
               int noOfCoefficients, IRtype ir_type, string outputRootName,
               double_pair_vec_t Fbands, double_pair_vec_t Abands,
               int wordlength, int grid_points, double_pair_t gain_bounds)
    : _solver(ScaLP::Solver(solverWishList)),
      noOfCoefficients(noOfCoefficients), solverWishList(solverWishList),
      threads(threads), timeout(timeout), outputRootName(outputRootName)

{
  // pass parameters to solver interface:
  _solver.timeout = timeout;
  _solver.threads = threads;

  this->band_tolerance = 0;
  this->eps_tolerance = 1e-10;

  // save values for ILP run
  this->timeout = timeout;
  this->threads = threads;
  this->gain_bounds = gain_bounds;

  useUniformFrequencyGrid = false;

  this->ir_type = ir_type;

  _no_init_grid_points = grid_points;

  useFixedGain = true;

  this->F_bands = Fbands;
  this->A_bands = Abands;
  this->wordlength = wordlength;

  band_parser();
  create_basis();

  IF_VERBOSE(1)
  cout << "Maximum coefficient wordlength = " << wordlength << endl;

  costModel = HIGH_LEVEL;
  cost_add = 1.0;
  cost_reg = 0.0; // by default, an MCM problem is considered
                  // (with no cost for pipelining)
  cost_fa = 1.0;
  cost_ff = 1.0;

  _no_of_pipeline_stages =
      -1; // -1 means determine automatically from target set

  gainInit = -1;       // negative value means ignore
  adderGraphInit = ""; // empty string means ignore
}

/*
 * This function parses the filter's frequency bands and creates
 * the proper _bounds functions.
 */
void FIROpt::band_parser() {
  // save the number of bands;
  _no_bands = F_bands.size();

  // Create the _bounds variable
  create_IR_bounds();

  // Scale the bands be the factor 2^B
  int factor = (1LL << wordlength);
  _A_bands_scaled.resize(_no_bands);
  for (int i = 0; i < _no_bands; ++i) {
    _A_bands_scaled[i].first =
        factor * (A_bands[i].first - this->band_tolerance);
    _A_bands_scaled[i].second =
        factor * (A_bands[i].second + this->band_tolerance);
  }

  IF_VERBOSE(2) {
    cout << "Initial constraints:" << endl;
    for (int i = 0; i < _no_bands; ++i) {
      cout << "for w in [" << F_bands[i].first << ", " << F_bands[i].second
           << "] : [" << A_bands[i].first << ", " << A_bands[i].second << "]."
           << endl;
    }
    cout << "Scaled constraints:" << endl;
    for (int i = 0; i < _no_bands; ++i) {
      cout << "for w in [" << F_bands[i].first << ", " << F_bands[i].second
           << "] : [" << _A_bands_scaled[i].first << ", "
           << _A_bands_scaled[i].second << "]." << endl;
    }
  }
}

void FIROpt::prepare_problem() {

  IF_VERBOSE(3)
  cout << "...preparing the data for the optimization problem" << endl;
  /*
   * compute the frequency grids omega
   */
  if (useUniformFrequencyGrid)
    generate_uniform_grid();
  else // non-uniform (AFP) grid
    generate_AFP_grid();

  // add extra points, if provided by the user
  augment_grid();

  _c_max = (1LL << wordlength);

  IF_VERBOSE(1) cout << "_c_max = " << _c_max << endl;

  /*
   * Prepare the target set which contans elements w=0...2^B-1
   * This set contains not only the fundamentals but also even w
   */

  bool boundsFound = generate_h_m_bounds();
  if (boundsFound) {
    for (pair<int_t, int_t> bound : _h_m_bounds) {
      for (int w_signed = bound.first; w_signed <= bound.second; w_signed++) {
        int w = abs(w_signed);
        if ((_no_of_pipeline_stages < 0) ||
            (adder_depth(w) <= _no_of_pipeline_stages))
          _target_set.insert(w);
      }
    }
  } else {
    for (int_t w = 0; w < _c_max; ++w) {
      if ((_no_of_pipeline_stages < 0) ||
          (adder_depth(w) <= _no_of_pipeline_stages))
        _target_set.insert(w);
    }
  }

  IF_VERBOSE(3) {
    std::cout << "Target set with B = " << wordlength << " is:\nw =";
    for (const int_t &it : _target_set) {
      std::cout << it << " ";
    }
    std::cout << endl;
  }

  /*
   * Determine the number of stages
   */
  if (_no_of_pipeline_stages < 0)
    // if not defined externaly, set it to the minimum necessary
    // for the whole target set
    _no_of_pipeline_stages = adder_depth(_target_set);

  IF_VERBOSE(1)
  cout << "Number of pipeline stages = " << _no_of_pipeline_stages << endl;

  // needed for ILP1 and ILP2
  // Resize the set h_m for m = 1,...,filterOrder

  _h_m.resize(noOfCoefficients);
}

void FIROpt::generate_uniform_grid() {
  _omega.resize(_no_bands);
  int_vec_t band_grid_points;
  band_grid_points.resize(_no_bands);
  double totalSize{0.0};
  for (int band_i{0}; band_i < _no_bands; ++band_i)
    totalSize += F_bands[band_i].second - F_bands[band_i].first;
  band_grid_points[_no_bands - 1] = _no_init_grid_points;
  for (int band_i{0}; band_i < _no_bands - 1; ++band_i) {
    band_grid_points[band_i] =
        floor((F_bands[band_i].second - F_bands[band_i].first) / totalSize *
              _no_init_grid_points);
    band_grid_points[_no_bands - 1] -= band_grid_points[band_i];
  }
  for (int band_i = 0; band_i < _no_bands; ++band_i) {
    _omega[band_i].resize(band_grid_points[band_i]);
    double step = (F_bands[band_i].second - F_bands[band_i].first) /
                  (band_grid_points[band_i] - 1);
    _omega[band_i][0] = F_bands[band_i].first;
    for (int i = 1; i < band_grid_points[band_i] - 1; ++i) {
      _omega[band_i][i] = _omega[band_i][i - 1] + step;
    }
    _omega[band_i][band_grid_points[band_i] - 1] = F_bands[band_i].second;
  }
}

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixXq;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXq;

void generateVandermondeMatrix(MatrixXq &A, std::size_t degree,
                               std::vector<double> &meshPoints,
                               std::function<double(double)> &weightFunction) {

  A.resize(degree + 1u, meshPoints.size());
  for (std::size_t i = 0u; i < meshPoints.size(); ++i) {
    double pointWeight = weightFunction(meshPoints[i]);
    A(0u, i) = 1;
    A(1u, i) = meshPoints[i];
    for (std::size_t j = 2u; j <= degree; ++j)
      A(j, i) = meshPoints[i] * A(j - 1u, i) * 2 - A(j - 2u, i);
    for (std::size_t j = 0u; j <= degree; ++j)
      A(j, i) *= pointWeight;
  }
}

// approximate Fekete points
void AFP(std::vector<double> &points, MatrixXq &A,
         std::vector<double> &meshPoints) {
  VectorXq b = VectorXq::Ones(A.rows());
  b(0) = 2;
  VectorXq y = A.colPivHouseholderQr().solve(b);

  for (std::size_t i = 0u; i < y.rows(); ++i)
    if (y(i) != 0.0)
      points.push_back(meshPoints[i]);
  std::sort(points.begin(), points.end(),
            [](const double &lhs, const double &rhs) { return lhs < rhs; });
}

void generateWAM(std::vector<double> &wam, double_pair_vec_t &chebyBands,
                 std::size_t degree) {
  std::vector<double> chebyNodes(degree + 2u);
  generateEquidistantNodes(chebyNodes, degree + 1u);
  applyCos(chebyNodes, chebyNodes);
  for (std::size_t i = 0u; i < chebyBands.size(); ++i) {
    if (chebyBands[i].first != chebyBands[i].second) {
      std::vector<double> bufferNodes(degree + 2u);
      changeOfVariable(bufferNodes, chebyNodes, chebyBands[i].first,
                       chebyBands[i].second);
      for (auto &it : bufferNodes)
        wam.push_back(it);
    } else
      wam.push_back(chebyBands[i].first);
  }
}

void generateAM(std::vector<double> &am, double_pair_vec_t &chebyBands,
                std::size_t degree) {
  std::vector<double> equiNodes(degree * degree);
  for (std::size_t i{0u}; i < degree * degree; ++i) {
    equiNodes[i] = -1.0 + (double)i / (degree * degree - 1) * 2;
  }
  for (std::size_t i = 0u; i < chebyBands.size(); ++i) {
    if (chebyBands[i].first != chebyBands[i].second) {
      std::vector<double> bufferNodes(degree * degree);
      changeOfVariable(bufferNodes, equiNodes, chebyBands[i].first,
                       chebyBands[i].second);
      for (auto &it : bufferNodes)
        am.push_back(it);
    } else
      am.push_back(chebyBands[i].first);
  }
}

void FIROpt::generate_AFP_grid() {
  // map the frequency bands to [-1,1] so that they can be used with
  // the AFP code framework
  double_pair_vec_t chebyBands;
  for (int i = _no_bands - 1; i >= 0; --i)
    chebyBands.push_back(std::make_pair(cos(M_PI * F_bands[i].second),
                                        cos(M_PI * F_bands[i].first)));
  MatrixXq V;
  std::vector<double> initialDiscretization;
  generateWAM(initialDiscretization, chebyBands, _no_init_grid_points);

  std::function<double(double)> weight = [](double x) -> double { return 1.0; };
  generateVandermondeMatrix(V, _no_init_grid_points, initialDiscretization,
                            weight);

  std::vector<double> finalDiscretization;
  AFP(finalDiscretization, V, initialDiscretization);
  // change back the points to the [0,1] normalized setting
  for (std::size_t i{0u}; i < finalDiscretization.size(); ++i)
    finalDiscretization[i] = acos(finalDiscretization[i]) / M_PI;

  std::sort(begin(finalDiscretization), end(finalDiscretization));

  // handle some change of variable numerical issues
  for (std::size_t i{0u}; i < finalDiscretization.size(); ++i)
    for (std::size_t j{0u}; j < F_bands.size(); ++j) {
      if (fabs(finalDiscretization[i] - F_bands[j].first) < 1e-14)
        finalDiscretization[i] = F_bands[j].first;
      if (fabs(finalDiscretization[i] - F_bands[j].second) < 1e-14)
        finalDiscretization[i] = F_bands[j].second;
    }

  // add frequency band edges that were not added in the discretization
  for (auto &it : F_bands) {
    finalDiscretization.push_back(it.first);
    finalDiscretization.push_back(it.second);
  }
  std::sort(begin(finalDiscretization), end(finalDiscretization));
  finalDiscretization.erase(
      unique(begin(finalDiscretization), end(finalDiscretization)),
      end(finalDiscretization));

  // parse the entire AFP grid and add it to the vector of grid points
  // of each band
  int b_idx = 0;
  _omega.clear();
  _omega.resize(F_bands.size());
  for (std::size_t i{0u}; i < finalDiscretization.size(); ++i) {
    if (finalDiscretization[i] > F_bands[b_idx].second)
      ++b_idx;
    _omega[b_idx].push_back(finalDiscretization[i]);
  }
}

void FIROpt::create_IR_bounds() {
  IF_VERBOSE(3)
  cout << "..."
       << "defining the functions of bounds on the "
       << "frequency response" << endl;
  _bounds.resize(_no_bands);
  for (int i = 0; i < _no_bands; ++i)
    _bounds[i] = [i](double omega, double A_bound) -> double {
      return A_bound;
    };
}

/* This function computes the basis functions c(m,omega) used in the paper.
 */
void FIROpt::create_basis() {
  IF_VERBOSE(3)
  cout << "..."
       << "defining the basis functions" << endl;
  _basis.resize(noOfCoefficients);
  switch (ir_type) {
  case SYM_EVEN: {
    _basis[0] = [](double omega) -> double { return 1.0; };
    for (int_t i{1u}; i < noOfCoefficients; ++i)
      _basis[i] = [i](double omega) -> double {
        return 2.0 * cos(omega * M_PI * i);
      };
    break;
  }
  case SYM_ODD: {
    for (int_t i{0u}; i < noOfCoefficients; ++i)
      _basis[i] = [i](double omega) -> double {
        return 2.0 * cos(omega * M_PI * (i + .5));
      };
    break;
  }
  case ASYM_EVEN: {
    for (int_t i{0u}; i < noOfCoefficients; ++i)
      _basis[i] = [i](double omega) -> double {
        return 2.0 * sin(omega * M_PI * (i + 1));
      };
    break;
  }
  default: { // ASYM_ODD
    for (int_t i{0u}; i < noOfCoefficients; ++i)
      _basis[i] = [i](double omega) -> double {
        return 2.0 * sin(omega * M_PI * (i + .5));
      };
    break;
  }
  }
}

bool FIROpt::generate_h_m_bounds() {
  // use a dense grid of frequency points for performing the projections
  double_double_vec_t projGrid;
  projGrid.resize(_no_bands);
  int bandGridPoints = 2000;
  for (int band_i = 0; band_i < _no_bands; ++band_i) {
    projGrid[band_i].resize(bandGridPoints);
    double step =
        (F_bands[band_i].second - F_bands[band_i].first) / (bandGridPoints - 1);
    projGrid[band_i][0] = F_bands[band_i].first;
    for (int i = 1; i < bandGridPoints - 1; ++i) {
      projGrid[band_i][i] = projGrid[band_i][i - 1] + step;
    }
    projGrid[band_i][bandGridPoints - 1] = F_bands[band_i].second;
  }

  LPCoeffBound lpcoeff = LPCoeffBound(
      solverWishList, threads, timeout, noOfCoefficients, ir_type, wordlength,
      projGrid, _no_bands, _A_bands_scaled, useFixedGain);

  if (lpcoeff.compute_bounds()) {
    _h_m_bounds.resize(noOfCoefficients);
    for (int i = 0; i < _h_m_bounds.size(); i++) {
      IF_VERBOSE(4)
      cout << lpcoeff.h_bounds[i].first << "<= h_" << i
           << " <=" << lpcoeff.h_bounds[i].second << endl;
      _h_m_bounds[i] = lpcoeff.h_bounds[i];

      if (_h_m_bounds[i].first < -_c_max + 1) {
        cout << "Warning: Lower bound of h(" << i
             << ")=" << _h_m_bounds[i].first
             << " less than minimum representable number (" << -_c_max
             << ") given by the chosen word size." << endl;
        _h_m_bounds[i].first = -_c_max + 1;
      }
      if (_h_m_bounds[i].second > _c_max - 1) {
        cout << "Warning: Upper bound of h(" << i
             << ")=" << _h_m_bounds[i].second
             << " larger than maximum representable number (" << _c_max
             << ") given by the chosen word size." << endl;
        _h_m_bounds[i].second = _c_max - 1;
      }
    }
    return true;
  } else {
    IF_VERBOSE(1)
    cout << "Could not determine the bounds on the impulse response." << endl;
    return false;
  }
}

void FIROpt::initialize_start_values() {
  // in case a greedy search is initiated, reset the greedyCalls counter
  greedyCalls = 0;

  if (_solver.featureSupported(ScaLP::Feature::WARMSTART)) {
    cout << "Solver supports warm start feature, turning on" << endl;
    _solver.warmStart = true;

    if (coeffInit.size() > 0) {
      cout << "Using given coefficients for initialization" << endl;

      if (_h_m.size() != coeffInit.size()) {
        cout << "Error: number of provided coefficients for initialization ("
             << coeffInit.size()
             << ") does not match with expected coefficient count ("
             << _h_m.size() << ")" << endl;
        exit(-1);
      }

      startValues.values.clear();
      for (int i = 0; i < coeffInit.size(); i++) {
        startValues.values[_h_m[i]] = coeffInit[i];
      }
    }

    if (gainInit > 0) {
      if ((gainInit > gain_bounds.second) || (gainInit < gain_bounds.first)) {
        cout << "Error: provided gain (" << gainInit
             << ") initialization out of range (" << gain_bounds.first
             << " ... " << gain_bounds.second << ")" << endl;
        exit(-1);
      }
      cout << "Using given gain for initialization" << endl;
      startValues.values[gain] = gainInit;
    }

    cout << "using start values:" << endl;
    cout << startValues.showSolutionVector(true) << endl;
  }
}

int FIROpt::getFilterOrder() {
  switch (ir_type) {
    int filterOrder;
  case SYM_EVEN:
    return 2 * (noOfCoefficients - 1);
  case SYM_ODD:
    return 2 * noOfCoefficients - 1;
  case ASYM_EVEN:
    return 2 * noOfCoefficients;
  case ASYM_ODD:
    return 2 * noOfCoefficients - 1;
  }
}

bool FIROpt::check_gain(double gainVal, bool printInfo) {
  bool valid = true;
  std::vector<double> s_h_m;
  double epsilon = std::numeric_limits<double>::epsilon();

  for (auto it : i_h_m) {
    s_h_m.push_back((double)it / (pow(2, wordlength) * gainVal));
  }

  for (auto it : s_h_m) {
    if (std::isnan(it) or std::isinf(it))
      return false;
  }

  // TODO: use a Clenshaw/Horner-type algorithm for the evaluation
  // of the frequency response
  auto basisFuncs = _basis;
  std::function<double(double)> H = [basisFuncs,
                                     s_h_m](double omega) -> double {
    double result = 0;
    for (int i{0}; i < basisFuncs.size(); ++i)
      result += basisFuncs[i](omega) * s_h_m[i];
    return result;
  };

  int bandCount = 16 * noOfCoefficients > 10000 ? 16 * noOfCoefficients : 10000;
  for (int i{0}; i < F_bands.size(); ++i) {
    if (printInfo) {
      std::cout << "Checking band " << i << " [" << F_bands[i].first << ", "
                << F_bands[i].second << "] with bounds [" << A_bands[i].first
                << ", " << A_bands[i].second << "]...\n";
    }
    double omegaBuff;
    // add some small tolerance for the feasibility checks
    for (int j{0}; j < bandCount && valid; ++j) {
      omegaBuff = F_bands[i].first +
                  (F_bands[i].second - F_bands[i].first) / (bandCount - 1) * j;
      if (H(omegaBuff) < A_bands[i].first - epsilon * 5) {
        valid = false;
        if (printInfo) {
          std::cout << "Dirty validation FAILED:\n"
                    << "\t-> frequency point issue: " << omegaBuff << std::endl
                    << "\t" << A_bands[i].first << " <= " << H(omegaBuff)
                    << " <= " << A_bands[i].second << " does not hold!\n";
          std::cout << "\tH(omegaBuff) - A_bands[i].first  = "
                    << H(omegaBuff) - A_bands[i].first << endl;
        }

      } else if (H(omegaBuff) > A_bands[i].second + epsilon * 5) {
        valid = false;
        if (printInfo) {
          std::cout << "Dirty validation FAILED:\n"
                    << "\t-> frequency point issue: " << omegaBuff << std::endl
                    << "\t" << A_bands[i].first << " <= " << H(omegaBuff)
                    << " <= " << A_bands[i].second << " does not hold!\n";
          std::cout << "\tA_bands[i].second - H(omegaBuff)  = "
                    << A_bands[i].second - H(omegaBuff) << endl;
        }
      }
    }
  }

  return valid;
}

bool FIROpt::dirty_validate(double &gainVal) {

  bool valid = true;

  if (!useFixedGain) {
    double epsilon = std::numeric_limits<double>::epsilon();
    std::vector<double> s_h_m;

    pair<int, double> maxError = max_error(gainVal);

    for (auto it : i_h_m) {
      s_h_m.push_back((double)it / (pow(2, wordlength) * gainVal));
    }

    std::cout << std::setprecision(20) << "Gain value: " << gainVal
              << std::setprecision(7) << std::endl;
    std::cout << "Wordlength: " << wordlength << std::endl;

    for (auto it : s_h_m) {
      if (std::isnan(it) or std::isinf(it))
        return false;
    }

    auto basisFuncs = _basis;
    std::function<double(double)> H = [basisFuncs,
                                       s_h_m](double omega) -> double {
      double result = 0;
      for (int i{0}; i < basisFuncs.size(); ++i)
        result += basisFuncs[i](omega) * s_h_m[i];
      return result;
    };

    std::vector<pair<int, double>> maxErrors = max_errors(gainVal);

    if (maxErrors.size() > 1) {

      bool underflow = false;
      bool overflow = false;
      for (auto &err : maxErrors) {
        if ((H(err.second) < A_bands[err.first].first && H(err.second) > 0) or
            (H(err.second) > A_bands[err.first].second && H(err.second) < 0))
          underflow = true;
        else
          overflow = true;
      }
      if (underflow and overflow) {
        cout << "Result both underflows and overflows; cannot tweak gain\n";
        valid = check_gain(gainVal, true);
        return valid;
      }
    }

    bool gainTweak = false;
    std::vector<double> tols = {0,     1e-16, 1e-15, 1e-14, 1e-13, 1e-12,
                                1e-11, 1e-10, 1e-9,  1e-8,  1e-8,  1e-7,
                                1e-6,  1e-5,  1e-4,  1e-3,  1e-2,  1e-1};
    bool underflowP =
        (H(maxError.second) > A_bands[maxError.first].first - 1e-2) and
        (H(maxError.second) < A_bands[maxError.first].first) and
        (H(maxError.second) > 0);
    bool underflowN =
        (H(maxError.second) > A_bands[maxError.first].second) and
        (H(maxError.second) < A_bands[maxError.first].second + 1e-2) and
        (H(maxError.second) < 0);
    bool overflowP =
        (H(maxError.second) > A_bands[maxError.first].second) and
        (H(maxError.second) < A_bands[maxError.first].second + 1e-2) and
        (H(maxError.second) > 0);
    bool overflowN =
        (H(maxError.second) > A_bands[maxError.first].first - 1e-2) and
        (H(maxError.second) < A_bands[maxError.first].first) and
        (H(maxError.second) < 0);
    if (underflowP or underflowN) {
      gainTweak = true;
      std::cout << "Underflow at " << maxError.second << " with error = "
                << (underflowP
                        ? H(maxError.second) - A_bands[maxError.first].first
                        : A_bands[maxError.first].second - H(maxError.second))
                << std::endl;
      s_h_m.clear();
      for (auto it : i_h_m)
        s_h_m.push_back((double)it / (pow(2, wordlength)));
      H = [basisFuncs, s_h_m](double omega) -> double {
        double result = 0;
        for (int i{0}; i < basisFuncs.size(); ++i)
          result += basisFuncs[i](omega) * s_h_m[i];
        return result;
      };
      bool gainPassed = false;

      s_h_m.clear();
      for (auto it : i_h_m)
        s_h_m.push_back((double)it / (pow(2, wordlength)));
      H = [basisFuncs, s_h_m](double omega) -> double {
        double result = 0;
        for (int i{0}; i < basisFuncs.size(); ++i)
          result += basisFuncs[i](omega) * s_h_m[i];
        return result;
      };

      double maxOmega = maxError.second;
      bool feasibleCheck = true;
      double baseVal;
      if (underflowP)
        baseVal = H(maxError.second) / A_bands[maxError.first].first;
      else
        baseVal = H(maxError.second) / A_bands[maxError.first].second;

      for (size_t it{0u}; it < tols.size() && !gainPassed && feasibleCheck;
           ++it) {
        std::cout << "Changing gain from " << gainVal << " to ";
        double newGainVal = baseVal - tols[it];
        std::cout << newGainVal << std::endl;
        gainPassed = check_gain(newGainVal);
        s_h_m.clear();
        for (auto hVal : i_h_m)
          s_h_m.push_back((double)hVal / (pow(2, wordlength) * newGainVal));
        H = [basisFuncs, s_h_m](double omega) -> double {
          double result = 0;
          for (int i{0}; i < basisFuncs.size(); ++i)
            result += basisFuncs[i](omega) * s_h_m[i];
          return result;
        };
        maxError = max_error(newGainVal);
        underflowP = H(maxError.second) < A_bands[maxError.first].first and
                     H(maxError.second) > 0;
        underflowN = H(maxError.second) > A_bands[maxError.first].second and
                     H(maxError.second) < 0;
        overflowP = H(maxError.second) > A_bands[maxError.first].second and
                    H(maxError.second) > 0;
        overflowN = H(maxError.second) < A_bands[maxError.first].first and
                    H(maxError.second) < 0;
        if (underflowP or underflowN) {
          std::cout << "Underflow at " << maxError.second << " with error = "
                    << (underflowP
                            ? H(maxError.second) - A_bands[maxError.first].first
                            : A_bands[maxError.first].second -
                                  H(maxError.second))
                    << std::endl;
          if (abs(maxError.second - maxOmega) > 1e-3) {
            feasibleCheck = false;
            std::cout << "Position of the over/under flow moved from "
                      << maxOmega << " to " << maxError.second
                      << "! The solution is not feasible!\n";
          }
        } else if (overflowP or overflowN) {
          std::cout << "Overflow at " << maxError.second << " with error = "
                    << (overflowP ? H(maxError.second) -
                                        A_bands[maxError.first].second
                                  : A_bands[maxError.first].first -
                                        H(maxError.second))
                    << std::endl;
          if (abs(maxError.second - maxOmega) > 1e-3) {
            feasibleCheck = false;
            std::cout << "Position of the over/under flow moved from "
                      << maxOmega << " to " << maxError.second
                      << "! The solution is not feasible!\n";
          }
        }

        if (gainPassed) {
          gainVal = newGainVal;
          feasibleCheck = false;
        }
      }

    } else if (overflowP or overflowN) {
      gainTweak = true;
      std::cout << "Overflow at " << maxError.second << " with error = "
                << (overflowP
                        ? H(maxError.second) - A_bands[maxError.first].second
                        : A_bands[maxError.first].first - H(maxError.second))
                << std::endl;

      bool gainPassed = false;

      s_h_m.clear();
      for (auto it : i_h_m)
        s_h_m.push_back((double)it / (pow(2, wordlength)));
      H = [basisFuncs, s_h_m](double omega) -> double {
        double result = 0;
        for (int i{0}; i < basisFuncs.size(); ++i)
          result += basisFuncs[i](omega) * s_h_m[i];
        return result;
      };

      double maxOmega = maxError.second;
      bool feasibleCheck = true;
      double baseVal;
      if (overflowP)
        baseVal = H(maxError.second) / A_bands[maxError.first].second;
      else
        baseVal = H(maxError.second) / A_bands[maxError.first].first;
      for (size_t it{0u}; it < tols.size() && !gainPassed && feasibleCheck;
           ++it) {
        std::cout << "Changing gain from " << gainVal << " to ";
        double newGainVal = baseVal + tols[it];
        std::cout << newGainVal << std::endl;
        gainPassed = check_gain(newGainVal);

        s_h_m.clear();
        for (auto hVal : i_h_m)
          s_h_m.push_back((double)hVal / (pow(2, wordlength) * newGainVal));
        H = [basisFuncs, s_h_m](double omega) -> double {
          double result = 0;
          for (int i{0}; i < basisFuncs.size(); ++i)
            result += basisFuncs[i](omega) * s_h_m[i];
          return result;
        };
        maxError = max_error(newGainVal);
        underflowP = H(maxError.second) < A_bands[maxError.first].first and
                     H(maxError.second) > 0;
        underflowN = H(maxError.second) > A_bands[maxError.first].second and
                     H(maxError.second) < 0;
        overflowP = H(maxError.second) > A_bands[maxError.first].second and
                    H(maxError.second) > 0;
        overflowN = H(maxError.second) < A_bands[maxError.first].first and
                    H(maxError.second) < 0;
        if (underflowP or underflowN) {
          // add a bit of tolerance close to machine precision
          double underflowVal =
              (underflowP
                   ? H(maxError.second) - A_bands[maxError.first].first
                   : A_bands[maxError.first].second - H(maxError.second));
          if (abs(underflowVal) > epsilon * 5) {
            std::cout << "Underflow at " << maxError.second
                      << " with error = " << underflowVal << std::endl;
            if (abs(maxError.second - maxOmega) > 1e-3) {
              feasibleCheck = false;
              std::cout << "Position of the over/under flow moved from "
                        << maxOmega << " to " << maxError.second
                        << "! The solution is not feasible!\n";
            }
          } else {
            gainPassed = true;
          }
        } else if (overflowP or overflowN) {
          // add a bit of tolerance close to machine precision
          double overflowVal =
              (overflowP ? H(maxError.second) - A_bands[maxError.first].second
                         : A_bands[maxError.first].first - H(maxError.second));
          if (abs(overflowVal) > epsilon * 5) {
            std::cout << "Overflow at " << maxError.second
                      << " with error = " << overflowVal << std::endl;
            if (abs(maxError.second - maxOmega) > 1e-3) {
              feasibleCheck = false;
              std::cout << "Position of the over/under flow moved from "
                        << maxOmega << " to " << maxError.second
                        << "! The solution is not feasible!\n";
            }
          } else {
            gainPassed = true;
          }
        }

        if (gainPassed) {
          feasibleCheck = false;
          gainVal = newGainVal;
        }
      }
    }

    if (gainTweak) {
      s_h_m.clear();
      for (auto it : i_h_m)
        s_h_m.push_back((double)it / (pow(2, wordlength) * gainVal));
      H = [basisFuncs, s_h_m](double omega) -> double {
        double result = 0;
        for (int i{0}; i < basisFuncs.size(); ++i)
          result += basisFuncs[i](omega) * s_h_m[i];
        return result;
      };
    }
  }

  valid = check_gain(gainVal, false);
  return valid;
}

std::pair<int, double> FIROpt::max_error(double gainVal) {

  std::vector<double> s_h_m;

  for (auto it : i_h_m) {
    s_h_m.push_back((double)it / (pow(2, wordlength) * gainVal));
  }

  auto basisFuncs = _basis;
  std::function<double(double)> H = [basisFuncs,
                                     s_h_m](double omega) -> double {
    double result = 0;
    for (int i{0}; i < basisFuncs.size(); ++i)
      result += basisFuncs[i](omega) * s_h_m[i];
    return result;
  };

  int bandCount = 16 * noOfCoefficients > 10000 ? 16 * noOfCoefficients : 10000;
  std::pair<int, double> maxErrorPoint = std::make_pair(0, 0.0);
  double maxError = 0.0;
  bool greedify = false;
  for (int i{0}; i < F_bands.size(); ++i) {
    double omegaBuff;
    for (int j{0}; j < bandCount; ++j) {
      omegaBuff = F_bands[i].first +
                  (F_bands[i].second - F_bands[i].first) / (bandCount - 1) * j;
      if (H(omegaBuff) < A_bands[i].first) {
        double currError = A_bands[i].first - H(omegaBuff);
        if (currError > maxError) {
          greedify = true;
          maxError = currError;
          maxErrorPoint.first = i;
          maxErrorPoint.second = omegaBuff;
        }
      } else if (H(omegaBuff) > A_bands[i].second) {
        double currError = H(omegaBuff) - A_bands[i].second;
        if (currError > maxError) {
          greedify = true;
          maxError = currError;
          maxErrorPoint.first = i;
          maxErrorPoint.second = omegaBuff;
        }
      }
    }
  }

  return maxErrorPoint;
}

std::vector<std::pair<int, double>> FIROpt::max_errors(double gainVal) {
  std::vector<std::pair<int, double>> toAdd;
  std::vector<double> s_h_m;

  for (auto it : i_h_m) {
    s_h_m.push_back((double)it / (pow(2, wordlength) * gainVal));
  }

  auto basisFuncs = _basis;
  std::function<double(double)> H = [basisFuncs,
                                     s_h_m](double omega) -> double {
    double result = 0;
    for (int i{0}; i < basisFuncs.size(); ++i)
      result += basisFuncs[i](omega) * s_h_m[i];
    return result;
  };

  int bandCount = 16 * noOfCoefficients > 10000 ? 16 * noOfCoefficients : 10000;
  std::pair<int, double> maxErrorPoint = std::make_pair(0, 0.0);
  double maxError = 0.0;
  bool newzone = false;
  for (int i{0}; i < F_bands.size(); ++i) {
    double omegaBuff;
    for (int j{0}; j < bandCount; ++j) {
      omegaBuff = F_bands[i].first +
                  (F_bands[i].second - F_bands[i].first) / (bandCount - 1) * j;
      if (H(omegaBuff) < A_bands[i].first) {
        if (!newzone)
          newzone = true;
        double currError = A_bands[i].first - H(omegaBuff);
        if (currError > maxError) {
          maxError = currError;
          maxErrorPoint.first = i;
          maxErrorPoint.second = omegaBuff;
        }
      } else if (H(omegaBuff) > A_bands[i].second) {
        if (!newzone)
          newzone = true;
        double currError = H(omegaBuff) - A_bands[i].second;
        if (currError > maxError) {
          maxError = currError;
          maxErrorPoint.first = i;
          maxErrorPoint.second = omegaBuff;
        }
      } else {
        if (newzone) {
          toAdd.push_back(maxErrorPoint);
          newzone = false;
          maxErrorPoint = std::make_pair(0, 0.0);
          maxError = 0.0;
        }
      }
    }
    if (newzone) {
      toAdd.push_back(maxErrorPoint);
      newzone = false;
      maxErrorPoint = std::make_pair(0, 0.0);
      maxError = 0.0;
    }
  }

  return toAdd;
}

void FIROpt::augment_grid() {
  for (auto &it : extraPoints) {
    for (int i{0}; i < F_bands.size(); ++i) {
      if ((it >= F_bands[i].first) && (it <= F_bands[i].second)) {
        _omega[i].push_back(it);
        break;
      }
    }
  }
}