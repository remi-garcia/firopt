//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

//    This file is adapted from the PAGSuite project, a suite of optimization
//    tools to optimize pipelined adder graphs. It is developed at the
//    University of Kassel and maintained by Martin Kumm
//    (martin.kumm@informatik.hs-fulda.de). You can use and/or modify it for
//    research purposes, for commercial use, please ask for a license. For more
//    information please visit http://www.uni-kassel.de/go/pagsuite.

#ifndef TYPES_H
#define TYPES_H

#include "ScaLP/Solver.h"
#include "rpag_vec.h"
#include "triplet.h"
#include <functional>
#include <list>
#include <map>
#include <ostream>
#include <set>
#include <string>
#include <vector>

#define UNUSED(x) (void)(x)
#define MAX_INT32 2147483647LL;
#define MAX_UINT32 4294967295LL;

#define USE_NEW_NZ_COMPARE_FUNTION 0 // 1 = use it; 0 = use the old version

#define USE_TOPOLOGY_E 1 // if 0, the pair-pair topology e is not checked

// the default values which are used by the cost models to set the FF and Fa
// cost's
#define COST_FF_DEFAULT_ASIC 0.5
#define COST_FA_DEFAULT_ASIC 1
#define COST_FF_DEFAULT_FPGA 1
#define COST_FA_DEFAULT_FPGA 1

enum IRtype { SYM_EVEN, SYM_ODD, ASYM_EVEN, ASYM_ODD };

// typedefs for frequently used types and data structures
typedef long long int int_t;
typedef std::vector<int_t> int_vec_t;
typedef std::set<int_t> int_set_t;

typedef std::vector<int_set_t> pipeline_set_t;

typedef std::vector<int> sd_t;
typedef std::set<sd_t> sd_set_t;
typedef std::vector<sd_t> sd_vec_t;
typedef std::set<sd_vec_t> sd_vec_set_t;

typedef std::map<int_t, double> int_double_map_t;

typedef std::pair<int_t, int_t> int_pair_t;
typedef std::set<int_pair_t> int_pair_set_t;
typedef std::map<int_pair_t, double> int_pair_double_map_t;

typedef rpag_vec<int_t> vec_t;
typedef std::map<vec_t, double> vec_double_map_t;
typedef std::pair<vec_t, vec_t> vec_pair_t;
typedef std::set<vec_t> vec_set_t;
typedef std::set<vec_pair_t> vec_pair_set_t;
typedef std::vector<vec_set_t> pipeline_vec_set_t;
typedef std::map<vec_pair_t, double> vec_pair_double_map_t;

typedef triplet<int_t, int_t, int_t> int_triplet_t;
typedef std::set<int_triplet_t> int_triplet_set_t;
typedef std::map<int_triplet_t, double> int_triplet_double_map_t;
typedef triplet<vec_t, vec_t, vec_t> vec_triplet_t;
typedef std::set<vec_triplet_t> vec_triplet_set_t;
typedef std::map<vec_triplet_t, double> vec_triplet_double_map_t;

typedef std::vector<std::function<double(double)>> double_fun_vec_t;
typedef std::vector<double> double_vec_t;
typedef std::vector<std::vector<double>> double_double_vec_t;
typedef std::pair<double, double> double_pair_t;
typedef std::vector<double_pair_t> double_pair_vec_t;

// maps w to set of pairs (u,v) such that w=A(u,v)
typedef std::map<int_t, int_pair_set_t> int_int_pair_set_map_t;
// stores the maps for each stage
typedef std::vector<int_int_pair_set_map_t> int_int_pair_set_map_vec_t;

typedef std::vector<std::map<int_t, ScaLP::Variable>> int_variable_map_vec_t;
typedef std::vector<std::map<int_pair_t, ScaLP::Variable>>
    int_pair_variable_map_vec_t;
typedef std::vector<std::map<int_triplet_t, ScaLP::Variable>>
    int_triplet_variable_map_vec_t;

#endif // TYPES_H
