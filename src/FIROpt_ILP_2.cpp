//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "FIROpt_ILP_2.h"
#include "fundamental.h"
#include "types.h"
#include <fstream>
#include <iomanip>

#ifdef HAVE_PAGLIB
#include "pagsuite/adder_graph.h"
#endif // HAVE_PAGLIB

using std::cout;
using std::endl;
using std::list;
using std::ofstream;
using std::set;
using std::string;
using std::stringstream;
using std::vector;

FIROpt_ILP_2::FIROpt_ILP_2(list<string> solverWishList, int threads,
                           int timeout, int noOfCoefficients, IRtype ir_type,
                           string outputRootName, double_pair_vec_t Fbands,
                           double_pair_vec_t Abands, double_pair_t gain_bounds,
                           int wordlength, int grid_points)
    : FIROpt(solverWishList, threads, timeout, noOfCoefficients, ir_type,
             outputRootName, Fbands, Abands, wordlength, grid_points,
             gain_bounds) {
  greedyCalls = 0;
}

void FIROpt_ILP_2::compute_complete_pipeline_set(int_set_t *target_set) {

  _single_set_vec[0].insert(1);

  /*
   * Fill the last stage with the target_set coefficients
   */
  for (const int_t &iter : *target_set) {
    _single_set_vec[_no_of_pipeline_stages].insert(fundamental(iter));
  }

  /*
   * Compute the successor sets. However, shouldn't it be recursive bottom to
   * top?(Ana)
   */
  for (int s{1}; s <= _no_of_pipeline_stages - 1; s++) {
    if (s < _no_of_pipeline_stages) {
      compute_successor_set(&(_single_set_vec[s - 1]), _c_max,
                            &(_single_set_vec[s]),
                            false); // fill new current set
    }
  }
}

void FIROpt_ILP_2::compute_pair_map(bool filter_pipeline_set) {
  int_vec_t pair_count_vec(_no_of_pipeline_stages + 1, 0);

  pair_count_vec[0] = 1;

  for (int s{_no_of_pipeline_stages - 1}; s >= 0; --s) {
    int_set_t predecessor_set;
    for (auto &int_set_u_iter : _single_set_vec[s])

    {
      for (auto &int_set_w_iter : _single_set_vec[s + 1]) {
        int_set_t v_set;
        compute_successor_set(int_set_u_iter, int_set_w_iter, _c_max, &v_set,
                              false);
        for (auto &int_set_v_iter : v_set) {
          if (int_set_v_iter >= int_set_u_iter) {
            if (adder_depth(int_set_v_iter) <= s) {
              pair_count_vec[s + 1]++;
              _pair_map[s + 1][int_set_w_iter].insert(make_pair(
                  fundamental(int_set_u_iter), fundamental(int_set_v_iter)));

              if (filter_pipeline_set) {
                predecessor_set.insert(fundamental(int_set_u_iter));
                predecessor_set.insert(fundamental(int_set_v_iter));
              }
            }
          }
        }
      }
    }
    if (filter_pipeline_set && (s > 0)) {
      _single_set_vec[s].clear();
      _single_set_vec[s].insert(predecessor_set.begin(), predecessor_set.end());
    }
  }

  IF_VERBOSE(1) {
    cout << "no of pairs in for _c_max=" << _c_max << " in stages 0..."
         << _no_of_pipeline_stages << " : ";
    for (int s{0}; s <= _no_of_pipeline_stages; s++) {
      cout << pair_count_vec[s] << "   ";
    }
    cout << endl;
  }
}

void FIROpt_ILP_2::print_pair_map() {
  for (int s{1}; s <= _no_of_pipeline_stages; s++) {
    cout << endl << "tripplets in stage " << s << " :" << endl << endl;
    for (auto &pair_map_iter : _pair_map[s]) {
      cout << "{" << pair_map_iter.first << ",(" << pair_map_iter.second
           << ")} " << endl;
    }
  }
}

ScaLP::Term FIROpt_ILP_2::constraints_coefficients(int sign, int m) {
  ScaLP::Term term(0);

  if (sign != 0 && sign != 1)
    return term;

  if (sign == 1) {
    term = term + 0;
  }

  for (const int_t &w : _target_set) {
    if (w != 0) // required in current ScaLP version, todo: check why!
      term = term + w * _h_m_w[m][w];
  }

  if (sign == 0)
    term = -term;

  return term;
}

void FIROpt_ILP_2::prepare_problem_AT() {

  /*
   * Resizing the containers for
   *       single_set_vec - the vector of sets of w on each stage
   *       pair_map       - the vector of pairs (u,v) on each stage
   */
  _single_set_vec.resize(_no_of_pipeline_stages + 1);
  _pair_map.resize(_no_of_pipeline_stages + 1);

  /*
   * Resize the set sign_m for m = 1..filterOrder
   */
  _sign_m.resize(noOfCoefficients);

  /*
   * Resize the set h_m_w for m = 1..filterOrder
   * and w = 0...2^B - 1
   */
  _h_m_w.resize(noOfCoefficients);

  /*
   * Computing the pipeline single sets A
   */
  IF_VERBOSE(3) cout << "computing pipeline set..." << endl;
  IF_VERBOSE(3) cout << "for target set " << _target_set << endl;

  compute_complete_pipeline_set(&_target_set);

  for (int s{1}; s <= _no_of_pipeline_stages; s++) {
    IF_VERBOSE(3)
    cout << "stage " << s << ", single_set=" << _single_set_vec[s] << endl;
  }

  /*
   * Compute pairs (u,v) for each stage, i.e., sets T^s
   */
  IF_VERBOSE(3) cout << "computing pairs..." << endl;
  compute_pair_map();

  for (int s{1}; s <= _no_of_pipeline_stages; s++) {
    IF_VERBOSE(3)
    cout << "stage " << s << ", reduced single_set=" << _single_set_vec[s]
         << endl;
  }
  IF_VERBOSE(4) print_pair_map();
}

void FIROpt_ILP_2::build_problem() {
  if (bigM <= 0) {
    bigM = (1LL << (wordlength + 1)); // set bigM to the minimum possible
    IF_VERBOSE(1) cout << "setting bigM to " << bigM << endl;
  }
  IF_VERBOSE(3)
  cout << "\n\n\n..."
       << "Building the optimization problem" << endl;

  IF_VERBOSE(3)
  cout << "\n\n\n..."
       << "Apply ILP formulation 2" << endl;

  // create adder and register variables of type ScaLP:
  for (int s{1}; s <= _no_of_pipeline_stages; s++) {
    for (auto &iter : _single_set_vec[s]) {
      stringstream strstra;
      strstra << "a_" << s << "_" << iter;

      _adder_variable_map_vec[s][iter] =
          ScaLP::newBinaryVariable(strstra.str());

      if (adder_depth(iter) < s) {
        stringstream strstrr;
        strstrr << "r_" << s << "_" << iter;

        _register_variable_map_vec[s][iter] =
            ScaLP::newBinaryVariable(strstrr.str());
      }
    }
  }

  // create pair variables:
  for (int s{2}; s <= _no_of_pipeline_stages; s++) {
    for (auto &pair_map_iter : _pair_map[s]) {
      for (auto &pair_set_iter : pair_map_iter.second) {
        int_t u = pair_set_iter.first;
        int_t v = pair_set_iter.second;
        int_pair_t uv_pair = make_pair<int_t, int_t>((int_t)u, (int_t)v);
        auto pair_map_it = _uv_pair_variable_map_vec[s].find(uv_pair);

        if (pair_map_it == _uv_pair_variable_map_vec[s].end()) {
          stringstream strstr;
          strstr << "x_" << s << "_" << u << "_" << v;
          // Question, shouldn't it be x^S_(u,v) variable?
          if (useRelaxation)
            _uv_pair_variable_map_vec[s][uv_pair] =
                ScaLP::newRealVariable(strstr.str(), 0.0, 1.0);
          // relaxation possible here due to C8
          else
            _uv_pair_variable_map_vec[s][uv_pair] =
                ScaLP::newBinaryVariable(strstr.str());
        }
      }
    }
  }

  /*
   * Create Transfer-Function related variables
   */

  // create sign_m:
  for (int m{0}; m < _sign_m.size(); ++m) {
    stringstream strstrsign;
    strstrsign << "sign_" << m;
    _sign_m[m] = ScaLP::newBinaryVariable(strstrsign.str());
  }

  i_h_m.resize(_h_m.size());
  // create h_m:
  for (int m{0}; m < _h_m.size(); ++m) {
    stringstream strstrh;
    strstrh << "h_" << m;
    if (useRelaxation)
      _h_m[m] = ScaLP::newRealVariable(strstrh.str());
    else
      _h_m[m] = ScaLP::newIntegerVariable(strstrh.str());
  }

  // create h_m_w:
  for (int m{0}; m < _h_m_w.size(); ++m) {
    for (const int_t &w : _target_set) {
      stringstream strstrhmw;
      strstrhmw << "h_" << m << "_" << w;
      _h_m_w[m][w] = ScaLP::newBinaryVariable(strstrhmw.str());
    }
  }

  // create gain variable:
  cout << "creating variable gain" << endl;
  gain = ScaLP::newRealVariable("gain", gain_bounds.first, gain_bounds.second);

  // create objective function:
  double cost_w_add = cost_add;
  double cost_w_reg = cost_reg;
  ScaLP::Term obj_term(0);

  // add multiplier block adder cost:
  if ((objective == TOTAL_ADDERS) || (objective == MULT_ADDERS)) {
    IF_VERBOSE(1)
    cout << "adding multiplier block adder cost to objective" << endl;
    for (int s{1}; s <= _no_of_pipeline_stages; ++s) {
      for (const int_t &iter : _single_set_vec[s]) {
        if (costModel == LOW_LEVEL) {
          // TODO: resolve the type problem!
          cost_w_add = (log2c_64(iter) + input_wordsize) * cost_fa;
        }
        obj_term = obj_term + cost_w_add * _adder_variable_map_vec[s][iter];

        auto regit = _register_variable_map_vec[s].find(iter);
        if (regit != _register_variable_map_vec[s].end()) {
          if (costModel == LOW_LEVEL) {
            // TODO: resolve the type problem!
            cost_w_reg = (log2c_64(iter) + input_wordsize) * cost_ff;
          }
          obj_term = obj_term + cost_w_reg * (*regit).second;
        }
      }
    }
  }
  if ((objective == TOTAL_ADDERS) || (objective == STRUCT_ADDERS)) {
    // add objective only when zero coefficients are possible at all
    if (_target_set.find(0) != _target_set.end()) {
      IF_VERBOSE(1) cout << "adding structural adder cost to objective" << endl;
      switch (ir_type) {
      case SYM_EVEN:
        obj_term = obj_term - _h_m_w[0][0];
        for (int m{1}; m < noOfCoefficients; m++) {
          obj_term = obj_term - 2 * _h_m_w[m][0];
        }
        break;
      case SYM_ODD:
        for (int m{0}; m < noOfCoefficients; m++) {
          obj_term = obj_term - 2 * _h_m_w[m][0];
        }
        break;
      case ASYM_EVEN:
        for (int m{1}; m < noOfCoefficients; m++) {
          obj_term = obj_term - 2 * _h_m_w[m][0];
        }
        break;
      case ASYM_ODD:
        for (int m{0}; m < noOfCoefficients; m++) {
          obj_term = obj_term - 2 * _h_m_w[m][0];
        }
        break;
      }
    } else {
      IF_VERBOSE(1)
      cout << "skipping adding of structural adder cost to "
           << " objective as no zero coefficient present" << endl;
    }
  }
  _solver.setObjective(ScaLP::minimize(obj_term));

  IF_VERBOSE(4) cout << "Objective function: " << obj_term << endl;

  initialize_start_values();

  /*
   * Create constraints
   */

  // Constraints C1a:
  // Mean the constraints on the filter's transfer function
  // These constraints are unified for each band.
  //
  // _bound[band](omega, delta) <= h_m[m] * c[m](omega) <= _bound[band](omega,
  // delta)
  //
  // For the moment, we assume the bounds as constants.
  int i{0};
  for (int band_i{0}; band_i < _no_bands; band_i++) {
    for (const double &omega : _omega[band_i]) {
      ScaLP::Term term_c1(0);

      double lower_bound =
          _bounds[band_i](omega, _A_bands_scaled[band_i].first);
      double upper_bound =
          _bounds[band_i](omega, _A_bands_scaled[band_i].second);

      for (int m{0}; m < noOfCoefficients; ++m) {
        term_c1 = term_c1 + _basis[m](omega) * _h_m[m];
      }
      IF_VERBOSE(4)
      cout << "C1a(#" << i << ") :" << lower_bound << gain << " <= " << term_c1
           << "<=" << upper_bound << gain << endl;
      i++;
      if (useFixedGain) {
        _solver << (lower_bound <= term_c1 <= upper_bound);
      } else {
        _solver << (0 <= term_c1 - lower_bound * gain);
        _solver << (term_c1 - upper_bound * gain <= 0);
      }
    }
  }

  // Constraint C1b
  IF_VERBOSE(3)
  cout << "Trying to determine the bounds on the impulse response..." << endl;
  for (int i{0}; i < _h_m_bounds.size(); i++) {
    IF_VERBOSE(4)
    cout << "C1b(#" << i << "): " << _h_m_bounds[i].first << "<= h_" << i
         << " <=" << _h_m_bounds[i].second << endl;

    _solver << (_h_m_bounds[i].first <= _h_m[i] <= _h_m_bounds[i].second);
  }

  // Constraint C2:
  if (!useFixedGain) {
    // TODO: this should not be necessary from the variable definition, fix this
    // in ScaLP!
    //  _solver << (gain_min <= gain <= gain_max);
    _solver << (gain_bounds.first - gain <=
                0); // this is better to debug as it leads to a valid lp file
    _solver << (gain - gain_bounds.second <= 0);
  }

  // Constraints C3a:
  // Mean the constraints on the filter coefficients
  i = 0;
  for (int m{0}; m < noOfCoefficients; ++m) {
    ScaLP::Term term_1 = constraints_coefficients(1, m);
    ScaLP::Term term_0 = constraints_coefficients(0, m);
    IF_VERBOSE(4)
    cout << "C3a(#" << i << ")\t if sign = 1 then h_" << m << "=" << term_1
         << endl;
    i++;
    IF_VERBOSE(4)
    cout << "C3a(#" << i << ")\t if sign = 0 then h_" << m << "=" << term_0
         << endl;
    i++;
    if (useIndicatorConstraints) {
      _solver << (_sign_m[m] == 1 then _h_m[m] - term_1 == 0);
      _solver << (_sign_m[m] == 0 then _h_m[m] - term_0 == 0);
    } else {
      _solver << (term_1 - bigM + bigM * _sign_m[m] - _h_m[m] <=
                  0); // if _sign_m[m]=true
      _solver << (_h_m[m] - term_1 - bigM + bigM * _sign_m[m] <=
                  0); // if _sign_m[m]=true
      _solver << (_h_m[m] + term_1 - bigM * _sign_m[m] <=
                  0); // if _sign_m[m]=false
      _solver << (-term_1 - bigM * _sign_m[m] - _h_m[m] <=
                  0); // if _sign_m[m]=false
    }
  }

  // Constraints C3b:
  // Limit coefficient selection booleans
  i = 0;
  for (int m{0}; m < noOfCoefficients; ++m) {
    ScaLP::Term term;

    for (const int_t &w : _target_set) {
      term = term + _h_m_w[m][w];
    }
    IF_VERBOSE(4) cout << "C3b(#" << i << ")\t" << term << " <= 1" << endl;
    i++;
    _solver << (term == 1);
  }

  // Constraints C4:
  // r_s_w + a_s_w >= 1/M * sum_{m=1}^M h_m_w     for all w=2^B - 1

  IF_VERBOSE(4)
  cout << "Constraints C4. They are applied only to the odd "
       << "elements of the target set." << endl;

  int s = _no_of_pipeline_stages;
  i = 0;
  for (const int_t &target_it :
       _target_set) // TODO: verify that this is correct
  {
    if (target_it != 0) {
      ScaLP::Term term_c4_left(0);
      ScaLP::Term term_c4_right(0);

      // constructing the left-hand side
      term_c4_left = _adder_variable_map_vec[s][fundamental(target_it)];
      auto reg = _register_variable_map_vec[s].find(fundamental(target_it));
      if (reg != _register_variable_map_vec[s].end())
        term_c4_left = term_c4_left + (*reg).second;

      // constructing the right-hand side
      double coeff = 1.0 / (double)noOfCoefficients;
      for (int m{0}; m < noOfCoefficients; ++m) {
        term_c4_right = term_c4_right + coeff * _h_m_w[m][target_it];
      }

      // constructing the constraint
      IF_VERBOSE(4)
      cout << "Constraint C4(#" << i << "): " << term_c4_left
           << ">=" << term_c4_right << endl;
      _solver << (term_c4_left - term_c4_right >= 0);

      i++;
    }
  }
  // Constraints C5: not necessary as the corresponding variables were not
  // created?
  IF_VERBOSE(4)
  cout << "Constraints C5 are not necessary because "
       << "corresponding variables were not created " << endl;

  // Constraints C6:
  // r_w^s - a_w^(s-1) - r_w^(s-1) <= 0
  // It means that a value w can be stored in a register only if it was computed
  // before
  i = 0;
  for (int s{2}; s <= _no_of_pipeline_stages; ++s) {
    ScaLP::Term term_c6(0);
    for (const int_t &iter : _single_set_vec[s]) {
      auto reg_s = _register_variable_map_vec[s].find(iter);
      if (reg_s != _register_variable_map_vec[s].end()) {
        term_c6 = (*reg_s).second;

        auto a_s_minus_one = _adder_variable_map_vec[s - 1].find(iter);
        if (a_s_minus_one != _adder_variable_map_vec[s - 1].end()) {
          term_c6 = term_c6 - (*a_s_minus_one).second;
        } else {
          IF_VERBOSE(5)
          cout << "In constraint C6 asked for an adder variable that "
               << "does not exist on previous stage." << endl;
        }

        auto reg_s_minus_one = _register_variable_map_vec[s - 1].find(iter);
        if (reg_s_minus_one != _register_variable_map_vec[s - 1].end())
          term_c6 = term_c6 - (*reg_s_minus_one).second;
        IF_VERBOSE(4)
        cout << "Constraint C6(#" << i << "): " << term_c6 << "<= 0" << endl;
        i++;
        _solver << (term_c6 <= 0);
      }
    }
  }

  // Constraints C7:
  // Means that if w has been computed with A(u,v) in stage s,
  // then the pair (u,v) has to be available in the
  // previous stage.
  // Attention, we do s=2..S-1 and s=S separately.
  // On the last iteration, we choose on fundemantals.

  int_set_t target_set_fundamentals;
  for (const int_t &target_it : _target_set) {
    if (target_it > 0)
      target_set_fundamentals.insert(fundamental(target_it));
  }

  i = 0;
  for (int s{2}; s <= _no_of_pipeline_stages - 1; ++s) {

    for (auto &pair_map_iter : _pair_map[s]) {
      ScaLP::Term term_c7(0);
      term_c7 = term_c7 + _adder_variable_map_vec[s][pair_map_iter.first];

      for (auto &pair_set_iter : pair_map_iter.second) {
        int_t u = pair_set_iter.first;
        int_t v = pair_set_iter.second;
        int_pair_t uv_pair = make_pair<int_t, int_t>((int_t)u, (int_t)v);
        term_c7 = term_c7 - _uv_pair_variable_map_vec[s][uv_pair];
      }
      IF_VERBOSE(4)
      cout << "Constraint C7(#" << i << "): " << term_c7 << "<= 0" << endl;
      i++;
      _solver << (term_c7 <= 0);
    }
  }

  // now, the constrant for s = S
  s = _no_of_pipeline_stages;
  if (s > 1) {
    for (auto &pair_map_iter : _pair_map[s]) {
      ScaLP::Term term_c7(0);
      auto w = target_set_fundamentals.find(pair_map_iter.first);
      if (w != target_set_fundamentals.end()) {
        term_c7 = term_c7 + _adder_variable_map_vec[s][pair_map_iter.first];

        for (auto &pair_set_iter : pair_map_iter.second) {
          int_t u = pair_set_iter.first;
          int_t v = pair_set_iter.second;
          int_pair_t uv_pair = make_pair<int_t, int_t>((int_t)u, (int_t)v);
          term_c7 = term_c7 - _uv_pair_variable_map_vec[s][uv_pair];
        }
        IF_VERBOSE(4)
        cout << "Constraint C7(#" << i << "): " << term_c7 << "<= 0" << endl;
        _solver << (term_c7 <= 0);
      }
    }
  }

  // Constraints C8
  // They mean that u and v are either stage or adder variables
  i = 0;
  for (int s{1}; s < _no_of_pipeline_stages; ++s) {
    for (auto &pair_map_it : _uv_pair_variable_map_vec[s + 1]) {
      {
        ScaLP::Term term_c8_a(0);
        term_c8_a = term_c8_a + pair_map_it.second -
                    _adder_variable_map_vec[s][pair_map_it.first.first];
        auto reg = _register_variable_map_vec[s].find(pair_map_it.first.first);
        if (reg != _register_variable_map_vec[s].end()) {
          term_c8_a = term_c8_a - (*reg).second;
        }
        IF_VERBOSE(4)
        cout << "Constraint C8a (#" << i << "): " << term_c8_a << "<= 0"
             << endl;
        _solver << (term_c8_a <= 0);
      }
      {
        ScaLP::Term term_c8_b(0);
        term_c8_b = term_c8_b + pair_map_it.second -
                    _adder_variable_map_vec[s][pair_map_it.first.second];
        auto reg = _register_variable_map_vec[s].find(pair_map_it.first.second);
        if (reg != _register_variable_map_vec[s].end()) {
          term_c8_b = term_c8_b - (*reg).second;
        }
        IF_VERBOSE(4)
        cout << "Constraint C8b (#" << i << "): " << term_c8_b << "<= 0"
             << endl;
        _solver << (term_c8_b <= 0);
      }
      i++;
    }
  }

  IF_VERBOSE(4) cout << "\n\n\n FINAL PROBLEM FORMULATION" << endl;

  IF_VERBOSE(4) cout << _solver.showLP() << endl;

  stringstream ss;
  ss << outputRootName << ".lp";
  _solver.writeLP(ss.str());
}

void FIROpt_ILP_2::initialize_start_values() {
  FIROpt::initialize_start_values();

  // Do ILP2 specific initializations
  if (_solver.featureSupported(ScaLP::Feature::WARMSTART)) {
    cout << "do ILP2 specific initializations" << endl;

    if (coeffInit.size() > 0) {
      for (int i{0}; i < coeffInit.size(); i++) {
        if (coeffInit[i] > 0)
          startValues.values[_sign_m[i]] = 1;
        else
          startValues.values[_sign_m[i]] = 0;

        // _h_m_w[i][w]
        for (const int_t &w : _target_set) {
          if (w == abs(coeffInit[i]))
            startValues.values[_h_m_w[i][w]] = 1;
          else
            startValues.values[_h_m_w[i][w]] = 0;
        }
      }
    }

    if (!adderGraphInit.empty()) {
#ifdef HAVE_PAGLIB
      PAGSuite::adder_graph_t pipelined_adder_graph;
      // pipelined_adder_graph.quiet = false; //enable debug output
      pipelined_adder_graph.quiet = true; // disable debug output, except errors

      IF_VERBOSE(3) cout << "parse graph..." << endl;
      bool validParse = pipelined_adder_graph.parse_to_graph(adderGraphInit);
      if (validParse) {
        IF_VERBOSE(3) cout << "check graph..." << endl;
        pipelined_adder_graph.check_and_correct(adderGraphInit);
      }
      IF_VERBOSE(3) pipelined_adder_graph.print_graph();
      pipelined_adder_graph.drawdot("pag_input_graph.dot");

      for (auto nodePtr : pipelined_adder_graph.nodes_list) {
        if (PAGSuite::is_a<PAGSuite::adder_subtractor_node_t>(*nodePtr)) {
          int w = ((PAGSuite::adder_subtractor_node_t *)nodePtr)
                      ->output_factor[0][0];
          int s = ((PAGSuite::adder_subtractor_node_t *)nodePtr)->stage;

          IF_VERBOSE(5)
          cout << "adder " << w << " is used in stage " << s << endl;
          if (_adder_variable_map_vec[s].find(w) !=
              _adder_variable_map_vec[s].end()) {
            startValues.values[_adder_variable_map_vec[s][w]] = 1;
          } else {
            cout << "Error: adder " << w << " in stage " << s
                 << " as specified in adder graph does not appear in "
                 << "search space. Maybe wrong word size used?" << endl;
            exit(-1);
          }
        } else if (PAGSuite::is_a<PAGSuite::register_node_t>(*nodePtr)) {
          int w = ((PAGSuite::register_node_t *)nodePtr)->output_factor[0][0];
          int s = ((PAGSuite::register_node_t *)nodePtr)->stage;

          IF_VERBOSE(5)
          cout << "register " << w << " is used in stage " << s << endl;
          if (_register_variable_map_vec[s].find(w) !=
              _register_variable_map_vec[s].end()) {
            startValues.values[_register_variable_map_vec[s][w]] = 1;
          } else {
            cout << "Error: register " << w << " in stage " << s
                 << " as specified in adder graph does not appear in "
                 << "search space. Maybe wrong word size used?" << endl;
            exit(-1);
          }
        }
      }
      /*
       */

      // set all variables to zero which have not been set:
      for (int s = 1; s <= _no_of_pipeline_stages; s++) {
        for (auto &w : _single_set_vec[s]) {
          if (startValues.values.find(_adder_variable_map_vec[s][w]) ==
              startValues.values.end()) {
            startValues.values[_adder_variable_map_vec[s][w]] = 0;
          }
          if ((adder_depth(w) < s) && w > 0) {
            if (startValues.values.find(_register_variable_map_vec[s][w]) ==
                startValues.values.end()) {
              startValues.values[_register_variable_map_vec[s][w]] = 0;
            }
          }
        }
      }
#else
      cout << "Error: adder graph specified but firopt was not linked against "
              "PAGLib"
           << endl;
      exit(-1);
#endif // HAVE_PAGLIB
    }

    cout << "using start values (incl. ILP2):" << endl;
    cout << startValues.showSolutionVector(false) << endl;
  }
}

void FIROpt_ILP_2::solve() {
  /*
   * Problem preparation:
   *  - generating grids
   *  - filling out the target set for given wordlengths
   *
   */
  prepare_problem();

  /*
   * Problem preparation:
   *  - resizing vectors for variable
   *  - computing the sets A^s and T^s
   *
   */
  prepare_problem_AT();

  // ILP2 specific problem preparation:

  // Resizing the containers for adder and register variables
  _adder_variable_map_vec.resize(_no_of_pipeline_stages + 1);
  _register_variable_map_vec.resize(_no_of_pipeline_stages + 1);

  // Resizing the container for (u,v) pair variables
  _uv_pair_variable_map_vec.resize(_no_of_pipeline_stages + 1);

  // build
  build_problem();

  // Try to solve
  _solver.quiet = false;
  _stat = _solver.solve(startValues);

  if (_stat == ScaLP::status::FEASIBLE or _stat == ScaLP::status::OPTIMAL or
      _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
    ScaLP::Result res = _solver.getResult();
    IF_VERBOSE(1) cout << res.showSolutionVector(false) << endl;
    for (unsigned i{0}; i < _h_m.size(); i++)
      i_h_m[i] = round(res.values[_h_m[i]]);
  }
}

void FIROpt_ILP_2::greedy_solve() {

  prepare_problem();

  prepare_problem_AT();

  _adder_variable_map_vec.resize(_no_of_pipeline_stages + 1);
  _register_variable_map_vec.resize(_no_of_pipeline_stages + 1);

  _uv_pair_variable_map_vec.resize(_no_of_pipeline_stages + 1);

  // build
  build_problem();

  double g, ig;
  ScaLP::Result res;
  do {
    if (greedyCalls == 0) {
      // Try to solve
      _solver.quiet = false;
      _stat = _solver.solve(startValues);

      if (_stat == ScaLP::status::FEASIBLE or _stat == ScaLP::status::OPTIMAL or
          _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
        res = _solver.getResult();
        for (unsigned i{0u}; i < _h_m.size(); i++)
          i_h_m[i] = round(res.values[_h_m[i]]);

        if (useFixedGain)
          g = 1.0;
        else
          g = res.values[gain];
      } else {
        return;
      }
      cout << "Intermediate gain = " << std::setprecision(20) << g
           << std::setprecision(7) << endl;
      cout << "Intermediate h = [";
      for (unsigned i{0u}; i < _h_m.size(); i++) {

        cout << (int)round(res.values[_h_m[i]]);
        if (i < _h_m.size() - 1) {
          cout << " ";
        }
      }
      cout << "]" << endl;

      if (dirty_validate(g)) {
        return;
      }
    }

    greedyCalls++;
    ig = g;

    auto toAdd = max_errors(ig);
    bool newPoints = false;
    for (auto &maxPoint : toAdd) {
      bool newPoint = true;
      for (const double &omega : _omega[maxPoint.first]) {
        if (maxPoint.second == omega)
          newPoint = false;
      }
      if (newPoint)
        _omega[maxPoint.first].push_back(maxPoint.second);
      newPoints = newPoints or newPoint;
    }
    if (!newPoints) {
      cout << "Numerical problem detected: trying to add only "
           << "points already present in the discretization!\n"
           << "Stopping the greedy search procedure...\n";
      return;
    }
    for (auto &maxPoint : toAdd) {
      ScaLP::Term term_c2(0);

      double lower_bound = _bounds[maxPoint.first](
          maxPoint.second, _A_bands_scaled[maxPoint.first].first);
      double upper_bound = _bounds[maxPoint.first](
          maxPoint.second, _A_bands_scaled[maxPoint.first].second);

      for (int m = 0; m < noOfCoefficients; ++m) {
        term_c2 = term_c2 + _basis[m](maxPoint.second) * _h_m[m];
      }

      if (useFixedGain) {
        _solver << (lower_bound <= term_c2 <= upper_bound);
      } else {
        _solver << (0 <= term_c2 - lower_bound * gain);
        _solver << (term_c2 - upper_bound * gain <= 0);
      }
    }

    // Try to solve
    _solver.quiet = false;
    _stat = _solver.solve(res); // solve again, initialize with last solution

    if (_stat == ScaLP::status::FEASIBLE or _stat == ScaLP::status::OPTIMAL or
        _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
      ScaLP::Result res = _solver.getResult();
      for (unsigned i{0u}; i < _h_m.size(); i++)
        i_h_m[i] = round(res.values[_h_m[i]]);

      if (useFixedGain)
        g = 1.0;
      else
        g = res.values[gain];

      cout << "Intermediate gain = " << std::setprecision(100) << g
           << std::setprecision(7) << endl;
      cout << "Intermediate h = [ ";
      for (unsigned i{0u}; i < _h_m.size(); i++) {

        cout << (int)round(res.values[_h_m[i]]);
        if (i < _h_m.size() - 1) {
          cout << " ";
        }
      }
      cout << "]" << endl;

      if (dirty_validate(g))
        return;
    } else {
      return;
    }

  } while (!dirty_validate(g));
}

int FIROpt_ILP_2::printSolution() {
  // also print the problem and solution information
  // inside a MATLAB script file
  stringstream ss;
  ss << outputRootName << ".m";
  ofstream outputInfo(ss.str());

  int exitCode = 1;

  int t;
  switch (ir_type) {
  case SYM_EVEN: {
    t = 1;
    break;
  }
  case SYM_ODD: {
    t = 2;
    break;
  }
  case ASYM_EVEN: {
    t = 3;
    break;
  }
  default: { // ASYM_ODD
    t = 4;
    break;
  }
  }

  cout << "type=" << t << endl;
  outputInfo << "type=" << t << ";" << endl;

  outputInfo << "wordlength=" << wordlength << ";\n";
  outputInfo << "fbands={";
  for (unsigned i{0u}; i < F_bands.size() - 1; ++i)
    outputInfo << "[" << F_bands[i].first << "," << F_bands[i].second << "],";
  outputInfo << "[" << F_bands[F_bands.size() - 1].first << ","
             << F_bands[F_bands.size() - 1].second << "]};\n";

  outputInfo << "abands={";
  for (unsigned i{0u}; i < A_bands.size() - 1; ++i)
    outputInfo << "[" << A_bands[i].first << "," << A_bands[i].second << "],";
  outputInfo << "[" << A_bands[A_bands.size() - 1].first << ","
             << A_bands[A_bands.size() - 1].second << "]};\n";

  cout << "greedyCalls=" << greedyCalls << endl;

  // get results
  cout << "The result is " << _stat << endl;
  ScaLP::Result res = _solver.getResult();
  IF_VERBOSE(4) cout << res << endl;
  if (_stat == ScaLP::status::FEASIBLE or _stat == ScaLP::status::OPTIMAL or
      _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
    vector<set<int_t>> pipeline_set(_no_of_pipeline_stages + 1);

    int noOfMultiplierBlockAdders = 0;
    for (unsigned s{0u}; s <= _no_of_pipeline_stages; s++) {
      for (auto &adder_variable_pair : _adder_variable_map_vec[s]) {
        if (round(res.values[adder_variable_pair.second]) != 0) {
          pipeline_set[s].insert(adder_variable_pair.first);
          noOfMultiplierBlockAdders++;
        }
      }
      for (auto &register_variable_pair : _register_variable_map_vec[s]) {
        if (round(res.values[register_variable_pair.second]) != 0) {
          pipeline_set[s].insert(register_variable_pair.first);
        }
      }
    }

    int filter_order = getFilterOrder();
    cout << "filter_order=" << filter_order << endl;

    int noOfTotalAdders =
        round(_solver.getResult().objectiveValue) + filter_order;
    if (objective == STRUCT_ADDERS) {
      // add MB adders as these are not contained in the objective:
      noOfTotalAdders += noOfMultiplierBlockAdders;
    }

    cout << "number_of_adders=" << noOfTotalAdders << endl;
    cout << "number_of_mult_adders=" << noOfMultiplierBlockAdders << endl;
    cout << "number_of_struct_adders="
         << noOfTotalAdders - noOfMultiplierBlockAdders << endl;

    // print results
    cout << "pipeline_set={";
    for (unsigned s{1u}; s <= _no_of_pipeline_stages; s++) {
      cout << "[";
      for (int_t w : pipeline_set[s]) {
        cout << w << " ";
      }
      cout << "]";
    }
    cout << "}" << endl;

    cout << "h=[";
    outputInfo << "h=[";
    for (unsigned i{0u}; i < _h_m.size(); i++) {

      cout << (int)round(res.values[_h_m[i]]);
      outputInfo << i_h_m[i];
      if (i < _h_m.size() - 1) {
        cout << " ";
        outputInfo << " ";
      }
    }
    cout << "]" << endl;
    outputInfo << "];\n";

    double g;
    if (useFixedGain) {
      g = 1.0;
    } else {
      g = res.values[gain];
    }

    cout << "Started naive validation...\n";
    if (dirty_validate(g)) {
      cout << "naive_validation=OK\n";
      exitCode = 0;
    } else {
      cout << "naive_validation=FAILED\n";
      exitCode = 10;
    }
    cout << std::setprecision(20);
    outputInfo << std::setprecision(20);
    cout << "gain=" << g << endl;
    outputInfo << "gain=" << g << ";" << endl;
    cout << std::setprecision(7);
    outputInfo << std::setprecision(7);

    outputInfo << "pipeline_set={";
    for (unsigned s{1u}; s <= _no_of_pipeline_stages; s++) {
      outputInfo << "[";
      for (int_t w : pipeline_set[s]) {
        outputInfo << w << " ";
      }
      if (s < _no_of_pipeline_stages)
        outputInfo << "],";
      else
        outputInfo << "]";
    }
    outputInfo << "};\n";
    outputInfo.close();

  } else {
    if (_stat == ScaLP::status::INFEASIBLE_OR_UNBOUND or
        _stat == ScaLP::status::INFEASIBLE or _stat == ScaLP::status::UNBOUND)
      exitCode = 11;
    else if (_stat == ScaLP::status::INVALID)
      exitCode = 12;
    else if (_stat == ScaLP::status::TIMEOUT_INFEASIBLE)
      exitCode = 13;
    else if (_stat == ScaLP::status::ERROR)
      exitCode = 14;
    else
      exitCode = 15;
  }

  return exitCode;
}
