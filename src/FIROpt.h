//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#pragma once

#include "LPCoeffBound.h"
#include "ScaLP/Solver.h"
#include "adder_depth.h"
#include "cheby.h"
#include "compute_successor_set.h"
#include "types.h"
#include <functional>
#include <list>
#include <map>
#include <ostream>
#include <set>
#include <string>
#include <vector>

class FIROpt {
public:
  // Comment: at this point, we should probably converge on how we do the
  // parsing; There is the pi factor we need to add somewhere when constructing
  // the bands. For the moment, I do not see it anywhere
  FIROpt(std::list<std::string> solverWishList, int threads = 0,
         int timeout = -1, int noOfCoefficients = 3, IRtype ir_type = SYM_EVEN,
         std::string outputRootName = "model",
         double_pair_vec_t Fbands = {std::make_pair(0.0, 0.3),
                                     std::make_pair(0.5, 1.0)},
         double_pair_vec_t Abands = {std::make_pair(11, 20),
                                     std::make_pair(-7, 7)},
         int wordlength = 3, int grid_points = 10,
         double_pair_t gain_bounds = std::make_pair(0.66, 1.34));

  std::list<std::string> solverWishList;

  IRtype ir_type;

  enum CostModel {
    HIGH_LEVEL, // The HIGH_LEVEL model simply counts the number of adders and
                // registers.
    LOW_LEVEL   // The LOW_LEVEL model is described on p.27 of Martin's thesis.
  };

  int wordlength; // the wordlength to be used

  int noOfCoefficients; // the order of the filter (M in the notation of the
                        // paper)

  std::vector<double> extraPoints; // extra points to add

  std::vector<int>
      coeffInit;   // coefficient values for initialization (warm start)
  double gainInit; // gain for initialization (warm start), negative value means
                   // ignore
  std::string adderGraphInit; // adder graph for initialization (warm start),
                              // empty string means ignore

  double band_tolerance; // eps to be added/subtracted from the A_bands
  double eps_tolerance;

  double_pair_vec_t
      A_bands; // constraints on the amplitude of the frequency response given
               // as pairs of upper and lower bounds
               // TODO: they can be given as functions of frequencies
               // vector<function<double(double)>> A_bands;

  double_pair_vec_t F_bands; // list of frequency bands that are defined as a
                             // list of right/left bounds [w_1, w_2]

  int input_wordsize = -1; // Needs to be et if LOW_LEVEL cost model is chosen.
                           // Not supported yet ith the class interface.

  bool useIndicatorConstraints = false;
  long bigM = -1;

  bool useUniformFrequencyGrid; // selects the frequency grid distribution
                                // between naive uniform or 'smart' non-uniform
                                // grid

  bool useFixedGain;

  enum ObjectiveType {
    TOTAL_ADDERS, // minimize the total number of adders
    MULT_ADDERS,  // minimize the adders in the multiplier block
    STRUCT_ADDERS // minimize the structural adders (e.g., sparse filters)
  };

  ObjectiveType objective;

  virtual void solve() = 0;

  virtual void greedy_solve() = 0;

  virtual int printSolution() = 0;

  /* Getter/Setter for some parameters changed during runtime */

  void setCostAdder(double cost) { cost_add = cost; }
  double getCostAdder() { return cost_add; }

  void setCostRegister(double cost) { cost_reg = cost; }
  double getCostRegister() { return cost_reg; }

  void setCostFA(double cost) { cost_fa = cost; }
  double getCostFA() { return cost_fa; }

  void setCostFF(double cost) { cost_ff = cost; }
  double getCostFF() { return cost_ff; }

  void setCostModel(CostModel costModel) { this->costModel = costModel; }
  CostModel getCostModel() { return costModel; }

  void setNoOfPipelineStages(int stages) { _no_of_pipeline_stages = stages; }
  double getNoOfPipelineStages() { return _no_of_pipeline_stages; }

  bool getUseRelaxation() { return useRelaxation; }

  void setUseRelaxation(bool useRelaxation) {
    this->useRelaxation = useRelaxation;
  }

  int getFilterOrder();
  bool dirty_validate(double &gainVal);

protected:
  // solver settings
  int threads;
  int timeout;

  bool useRelaxation; // if true, all integer/binary variables are relaxated to
                      // real variables

  CostModel costModel;
  double cost_add; // cost of an adder in HIGH_LEVEL cost model
  double cost_reg; // cost of a register in HIGH_LEVEL cost model
  double cost_fa;  // cost of a full adder in LOW_LEVEL cost model
  double cost_ff;  // cost of a flip flop in LOW_LEVEL cost model

  enum BoundType { UPPER, LOWER };

  int greedyCalls; // number of greedy calls

  double_pair_vec_t _A_bands_scaled;
  double_double_vec_t _omega; // the vector of vectors of frequency points for
                              // each band (omega.size() = no_bands)
  int _no_of_pipeline_stages; // max. pipeline depth
  int _c_max;                 // max. fundamental value
  int_set_t _target_set;      // vector w=(0...2^wordlength - 1)
  pipeline_set_t
      _single_set_vec; // vector(0...s_max) of sets of single elements w,
  int_int_pair_set_map_vec_t
      _pair_map; // vector(0...s_max) of maps that relate each element w
                 // to set of pairs (u,v) such that w=A(u,v)

  std::vector<ScaLP::Variable> _h_m;
  std::vector<int> i_h_m;

  ScaLP::Variable gain;    // gain of non-unitiy gain filters
  double_fun_vec_t _basis; // container for the basis functions
  std::vector<std::function<double(double, double)>>
      _bounds; // ontainer for the functions of bounds
  std::vector<std::pair<int_t, int_t>> _h_m_bounds;
  int_t _no_init_grid_points;
  unsigned long int _no_bands;
  std::string outputRootName;

  // constraints on the impulse response (constraint C1)

  void prepare_problem();
  ScaLP::Solver _solver;
  ScaLP::status _stat;
  ScaLP::Result _res;
  // container for storing initialization values
  // of variables (for warmStart feature)
  ScaLP::Result startValues;

  // bounds for the gain factor, in case it is variable
  double_pair_t gain_bounds;

  std::pair<int, double> max_error(double gainVal);
  std::vector<std::pair<int, double>> max_errors(double gainVal);

protected:
  virtual void initialize_start_values();

private:
  bool generate_h_m_bounds();
  void create_IR_bounds();
  void band_parser();
  void create_basis();
  void generate_uniform_grid();
  void generate_AFP_grid();
  void augment_grid();
  bool check_gain(double gainVal, bool printInfo = false);
};