//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#ifndef CHEBY_H_
#define CHEBY_H_

#include <algorithm>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <list>
#include <sstream>
#include <utility>
#include <vector>

void applyCos(std::vector<double> &out, std::vector<double> const &in);

void changeOfVariable(std::vector<double> &out, std::vector<double> const &in,
                      double &a, double &b);

void evaluateClenshaw(double &result, std::vector<double> &p, double &x,
                      double &a, double &b);

void evaluateClenshaw(double &result, std::vector<double> &p, double &x);

void evaluateClenshaw2ndKind(double &result, std::vector<double> &p, double &x);

void generateEquidistantNodes(std::vector<double> &v, std::size_t n);

void generateChebyshevPoints(std::vector<double> &v, std::size_t n);

void generateChebyshevCoefficients(std::vector<double> &c,
                                   std::vector<double> &fv, std::size_t n);

void derivativeCoefficients1stKind(std::vector<double> &derivC,
                                   std::vector<double> &c);

void derivativeCoefficients2ndKind(std::vector<double> &derivC,
                                   std::vector<double> &c);

#endif /* CHEBY_H_ */
