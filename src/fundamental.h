//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

//    This file is adapted from the PAGSuite project, a suite of optimization
//    tools to optimize pipelined adder graphs. It is developed at the
//    University of Kassel and maintained by Martin Kumm
//    (martin.kumm@informatik.hs-fulda.de). You can use and/or modify it for
//    research purposes, for commercial use, please ask for a license. For more
//    information please visit http://www.uni-kassel.de/go/pagsuite.

#ifndef FUNDAMENTAL_H
#define FUNDAMENTAL_H

#include "debug.h"
#include "type_class.h"

// returns the fundamental of x
inline int_t fundamental(int_t x) // inline and template doesn't work together
{
  if (x == 0)
    return 0;
  else {
    while ((x & 0x01) == 0)
      x >>= 1;
    return x;
  }
}
inline vec_t fundamental(vec_t x) // inline and template doesn't work together
{
  if (x == 0)
    return x; // if x is 0 we can return x as 0. (for correct dimensions)
  else {
    while ((x % 2) == 0) {
      x /= 2;
    }
    return x;
  }
}

template <class T> int fundamental_count(T x) {
  if (x == 0)
    return 0;
  else {
    int c = 0;
    while ((x % 2) == 0) {
      x /= 2;
      ++c;
    }
    return c;
  }
}

template <class T> T fundamental_count(T x, int &c) {
  c = 0;
  if (x == 0)
    return x; // if x is 0 we can return x as 0. (for correct dimensions in
              // vector case...)
  else {
    while ((x % 2) == 0) {
      x /= 2;
      ++c;
    }
    return x;
  }
}

// returns the fundamental of x without checking x!=0 !
inline int_t fundamental_unsave(int_t x) {
  while ((x & 0x01) == 0)
    x >>= 1;
  return x;
}

void fundamental(int_set_t *set);
void fundamental(vec_set_t *set);

#endif // FUNDAMENTAL_H
