//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

//    This file is adapted from the PAGSuite project, a suite of optimization
//    tools to optimize pipelined adder graphs. It is developed at the
//    University of Kassel and maintained by Martin Kumm
//    (martin.kumm@informatik.hs-fulda.de). You can use and/or modify it for
//    research purposes, for commercial use, please ask for a license. For more
//    information please visit http://www.uni-kassel.de/go/pagsuite.

#ifndef _DEBUG_H
#define _DEBUG_H

#include <cassert>
#include <iomanip>
#include <iostream>
#include <typeinfo>

#include "types.h"

#include <sstream>

#define DEBUG

using namespace std;

extern int global_verbose;

#define IF_VERBOSE(verbose)                                                    \
  if (global_verbose >= verbose)                                               \
    for (int i = 0; i < verbose; i++)                                          \
      cout << "  ";                                                            \
  if (global_verbose >= verbose)

ostream &operator<<(ostream &s, sd_t &sd);
ostream &operator<<(ostream &s, sd_set_t &msd_set);

template <typename T> ostream &operator<<(ostream &s, vector<T> &vec) {
  typename vector<T>::iterator iter;
  for (iter = vec.begin(); iter != vec.end();) {
    s << *iter;
    ++iter;
    if (iter != vec.end())
      s << "*,";
  }
  return s;
}

template <typename T> ostream &operator<<(ostream &s, const set<T> &st) {
  typename set<T>::iterator iter;
  for (iter = st.begin(); iter != st.end();) {
    if (typeid(T) != typeid(int_t))
      s << "[";
    s << (*iter);
    if (typeid(T) != typeid(int_t))
      s << "]";
    ++iter;
    if (iter != st.end())
      s << " ";
  }
  return s;
}

template <typename T> ostream &operator<<(ostream &s, list<T> &st) {
  typename list<T>::iterator iter;

  for (iter = st.begin(); iter != st.end();) {
    if (typeid(T) != typeid(int_t))
      s << "(";
    s << (*iter);
    if (typeid(T) != typeid(int_t))
      s << ")";
    ++iter;
    if (iter != st.end())
      s << " ";
  }
  return s;
}

template <typename T>
ostream &operator<<(ostream &s, vector<vector<set<T>>> &ps) {
  typename vector<vector<set<T>>>::iterator iter;
  s << "{";
  if (ps.size() > 0) {
    iter = ps.begin();
    s << "[" << *iter << "]";
    for (++iter; iter != ps.end(); ++iter) {
      s << ",[" << *iter << "]";
    }
  }
  s << "}";
  return s;
}

template <typename T> ostream &operator<<(ostream &s, vector<set<T>> &ps) {
  typename vector<set<T>>::iterator iter;
  s << "{";
  if (ps.size() > 0) {
    iter = ps.begin();
    s << "[" << *iter << "]";
    for (++iter; iter != ps.end(); ++iter) {
      s << ",[" << *iter << "]";
    }
  }
  s << "}";
  return s;
}

template <typename T>
ostream &operator<<(ostream &s, map<T, double> &int_double_map) {
  typename map<T, double>::iterator iter;
  for (iter = int_double_map.begin(); iter != int_double_map.end(); ++iter) {
    s << (*iter).first << ":" << (*iter).second << " ";
  }
  return s;
}

template <typename T> ostream &operator<<(ostream &s, pair<T, T> &int_pair) {
  s << "(" << int_pair.first << "," << int_pair.second << ")";
  return s;
}

template <typename T>
ostream &operator<<(ostream &s, map<pair<T, T>, double> &int_pair_double_map) {
  typename map<pair<T, T>, double>::iterator iter;
  pair<T, T> int_pair;
  for (iter = int_pair_double_map.begin(); iter != int_pair_double_map.end();
       ++iter) {
    int_pair = (*iter).first;
    s << int_pair << ":" << (*iter).second << " ";
  }
  return s;
}

template <typename T>
ostream &operator<<(ostream &s, set<pair<T, T>> &int_pair_set) {
  typename set<pair<T, T>>::iterator iter;
  pair<T, T> predecessor_pair;

  s << "{"; // output first element (without comma)
  // output remaining elements
  for (iter = int_pair_set.begin(); iter != int_pair_set.end(); ++iter) {
    predecessor_pair = *iter;
    if (iter != int_pair_set.begin())
      s << ",";
    s << predecessor_pair;
  }
  s << "}";
  return s;
}

template <class T> std::string toString(const T &t) {
  std::ostringstream stream;
  stream << t;
  return stream.str();
}

#endif //_DEBUG_H